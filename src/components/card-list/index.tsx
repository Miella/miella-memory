import React, { FC, useEffect, useState } from 'react';
import Card from './card';
import { mapStateToProps, mapDispatchToProps } from './container';
import { connect } from 'react-redux';
import style from './style.module.scss';
import { ICardImg } from '../../modules/interfaces/cardImage';

interface IComponentProps {
  initList: ICardImg[];
}

interface IContainerProps {
  cardListUpdated?: ICardImg[];
  currentShowList?: ICardImg[];
  addVisibleCard?: (data: ICardImg) => void;
  validatePeerFound?: () => void;
  hideCard?: () => void;
}

type IProps = Partial<IComponentProps> & Partial<IContainerProps>;

const CardList: FC<IProps> = (props) => {
  const {
    initList,
    currentShowList,
    addVisibleCard,
    validatePeerFound,
    hideCard,
  } = props;

  const [allCard, setAllCard] = useState<ICardImg[]>([]);

  useEffect(() => {
    if(initList) {
      setAllCard(initList);
    }
  }, [initList]);


  const handleClickCard = (card: ICardImg) => {
    if(currentShowList) {
      const sameCard = currentShowList.find(item => item.id === card.id);
      if (addVisibleCard  && currentShowList.length < 2 && !sameCard) {
        addVisibleCard(card);
      }
    }

  }

  useEffect(() => {
    if(currentShowList?.length === 2) {
      setTimeout(() => {
        // compare card
        if(currentShowList[0].name === currentShowList[1].name) {
          if (validatePeerFound) validatePeerFound();
        } else {
          if(hideCard) hideCard();
        }
      }, 500);
    }
  }, [currentShowList, hideCard, validatePeerFound]);



return (
  <div className={style.cardListContainer}>
    {allCard.length > 0 && allCard.map((item, index) => (
      <Card key={index} image={ item } clickCard={() => handleClickCard(item)} shouldShow={item.visible}/>
    ))}
  </div>
);
};

CardList.defaultProps = {};

export default connect(mapStateToProps,mapDispatchToProps)(CardList) as typeof CardList;

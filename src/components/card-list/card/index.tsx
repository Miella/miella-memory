import React, { FC } from 'react';
import { ICardImg } from '~/modules/interfaces/cardImage';
import style from './style.module.scss';

interface IComponentProps {
  image: ICardImg;
  shouldShow: boolean;
  clickCard: () => void;
}

interface IContainerProps {
  [x: string]: any;
}

type IProps = IComponentProps & Partial<IContainerProps>;

const Card: FC<IProps> = (props) => {
  const {
    image,
    shouldShow,
    clickCard,
  } = props;

  return (
    <div className={style.imageContainer} onClick={clickCard}>
        { shouldShow ?
          <div> <img src={image.url} alt={image.name} width={90}/> </div> :
          <div className={style.hideCard}>   </div>
        }
    </div>
  );
};

export default Card;

import { IApplicationState } from '../../setup/reducer';
import { getCurrentVisibleCard } from '../../modules/selectors/current-show-card';
import {
  AddVisibleCardStart,
  hideWrongPeerStart,
  validatePeerCardStart
} from '../../modules/actions';
import { ICardImg } from '~/modules/interfaces/cardImage';

export const mapStateToProps = (state: IApplicationState) => {
  const currentShowList = getCurrentVisibleCard(state);
  return {
    currentShowList,
  };
};

export const mapDispatchToProps = (dispatch: any) => ({
  addVisibleCard(data: ICardImg) {
    dispatch(AddVisibleCardStart(data));
  },
  validatePeerFound() {
    dispatch(validatePeerCardStart());
  },
  hideCard() {
    dispatch(hideWrongPeerStart());
  }
});


import { InitGameStart } from '../modules/actions';
import { IApplicationState } from '../setup/reducer';
import { getAllCard } from '../modules/selectors/get-all-card';

export const mapStateToProps = (state: IApplicationState) => {
  const initCardList = getAllCard(state);
  return {
    initCardList,
  };
};

export const mapDispatchToProps = (dispatch: any) => ({
  initializeGame() {
    dispatch(InitGameStart());
  },
});

import React, { FC, useEffect, useMemo, useState } from 'react';
import { makeStyles } from "@material-ui/core/styles";
import LinearProgress from "@material-ui/core/LinearProgress";
import Box from "@material-ui/core/Box";
import Modal from '@material-ui/core/Modal';
import style from './style.module.scss';
import { mapStateToProps } from './container';
import { connect } from 'react-redux';
import { mapDispatchToProps } from '../../components/card-list/container';

interface IComponentProps {
  [x: string]: any;
}

interface IContainerProps {
  gameStatus?: boolean;
}

type IProps = Partial<IComponentProps> & Partial<IContainerProps>;

const useStyles = makeStyles({
  root: {
    width: "100%"
  }
});

const ProgressBar: FC<IProps> = (props) => {
  const {
    gameStatus,
  } = props;
  const [bar, setBar] = useState(0);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const classes = useStyles();

  useEffect(() => {
    if (bar < 100) {
      setTimeout(() => {
        setBar(bar + 0.2);
      }, 100);
    }
  });

  useEffect(() => {
    if (bar >= 100) {
      setOpenModal(true);
    }
  }, [bar]);

  useEffect(() => {
    if(gameStatus) {
      setOpenModal(true);
    }
  }, [gameStatus]);

  const modalText = useMemo(() => {
    return gameStatus ? 'Congrats, You Win' : 'Game Over!';
  }, [gameStatus]);

  return (
    <>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={openModal}
      >
        <div className={style.modalText}> {modalText} </div>
      </Modal>

      <Box display="flex" alignItems="center" p={3}>
        <Box width="100%" mr={3}>
          <div className={classes.root}>
            <LinearProgress variant="determinate" value={bar} />
          </div>
        </Box>
      </Box>
    </>

  );
};

ProgressBar.defaultProps = {};

export default connect(mapStateToProps,mapDispatchToProps)(ProgressBar) as typeof ProgressBar;

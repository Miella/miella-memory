import { IApplicationState } from '../../setup/reducer';
import { getGameStatus } from '../../modules/selectors/get-game-status';

export const mapStateToProps = (state: IApplicationState) => {
  const gameStatus = getGameStatus(state);

  return {
    gameStatus
  };
};

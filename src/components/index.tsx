import React, { FC, useEffect } from 'react';
import ProgressBar from './progress-bar';
import CardList from './card-list';
import style from './style.module.scss';
import { mapStateToProps , mapDispatchToProps} from './container';
import { connect } from 'react-redux';
import { ICardImg } from '~/modules/interfaces/cardImage';

interface IComponentProps {
  [x: string]: any;
}

interface IContainerProps {
  initializeGame: () => void;
  initCardList: ICardImg[];
}

type IProps = IComponentProps & Partial<IContainerProps>;

const Memory: FC<IProps> = (props) => {
  const {
    initializeGame,
    initCardList,
  } = props;

  useEffect(() => {
    if (initializeGame) {
      initializeGame();
    }
  }, [initializeGame]);

  return (
    <div className={style.container}>
      {initCardList && initCardList.length > 0 && (
        <>
          <CardList initList={initCardList}/>
          <ProgressBar/>
        </>
      )}
    </div>
  );
};

Memory.defaultProps = {
  initList: []
};

export default connect(mapStateToProps, mapDispatchToProps)(Memory);

import {
  ADD_VISIBLE_CARD_SUCCESS,
  HIDE_WRONG_PEER_SUCCESS,
  INIT_GAME_SUCCESS,
  VALIDATE_PEER_CARD_SUCCESS
} from '../actions';

const initialState: any = {
  currentShowCard: [],
  allCard: [],
  done: false,
};

const memoryReducer = (state = initialState, action: any): any => {
  const { type, data } = action;
  switch (type) {
    case ADD_VISIBLE_CARD_SUCCESS:
    case HIDE_WRONG_PEER_SUCCESS: {
      const { currentVisible , newAllCard} = data;
      return {
        ...state,
        currentShowCard: [...currentVisible],
        allCard: [...newAllCard],
      };
    }
    case INIT_GAME_SUCCESS: {
      return {
        ...state,
        allCard: [...data],
      };
    }
    case VALIDATE_PEER_CARD_SUCCESS: {
      const { currentVisible , done} = data;
      return {
        ...state,
        currentShowCard: [...currentVisible],
        done,
      }
    }
    default:
      return state;
  }
};

export default memoryReducer;

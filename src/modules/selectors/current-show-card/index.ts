import { IApplicationState } from '../../../setup/reducer';
import { ICardImg} from '~/modules/interfaces/cardImage';

export const getCurrentVisibleCard = (state: IApplicationState): ICardImg[]  => {
  return state?.app?.currentShowCard || [];
};


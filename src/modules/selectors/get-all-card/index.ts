import { ICardImg} from '../../../modules/interfaces/cardImage';
import { IApplicationState } from '../../../setup/reducer';

export const getAllCard = (state: IApplicationState): ICardImg[]  => {
  return state?.app?.allCard || [];
};


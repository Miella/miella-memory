import { IApplicationState } from '../../../setup/reducer';

export const getGameStatus = (state: IApplicationState): boolean  => {
  return state?.app?.done;
};


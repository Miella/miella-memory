import { put, select, takeLatest } from 'redux-saga/effects';
import { VALIDATE_PEER_CARD_START, validatePeerCardSuccess } from '../../../modules/actions';
import { ICardImg } from '../../../modules/interfaces/cardImage';
import { getAllCard } from '../../../modules/selectors/get-all-card';

function* sagaWorker(action: any) {
  try {
    const allCard: ICardImg[] = yield select(getAllCard);
    const notVisible: ICardImg[] = yield allCard.find(item => item.visible === false);
    const done:boolean = yield notVisible ? false : true;
    yield put(validatePeerCardSuccess({ currentVisible: [] , done }));
  } catch (ex) {
    console.error(ex);
  }
}

function* validatePeerCardSaga() {
  yield takeLatest([VALIDATE_PEER_CARD_START], sagaWorker);
}

export default validatePeerCardSaga;

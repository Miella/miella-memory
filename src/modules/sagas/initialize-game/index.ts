import { put, takeLatest } from 'redux-saga/effects';
import { INIT_GAME_START, InitGameSuccess, InitGameError } from '../../../modules/actions';
import cardImgList from '../../../__mocks/data.mocks';
import { ICardImg } from '../../../modules/interfaces/cardImage';

function* sagaWorker(action: any) {
  try {
    const shuffledList: ICardImg[] = yield cardImgList
    .map(item => ({ item, sort: Math.random() }))
    .sort((a, b) => a.sort - b.sort)
    .map(({ item }) => item);

    yield put(InitGameSuccess(shuffledList));

  } catch (ex) {
    console.error(ex);
    yield put(InitGameError());
  }
}

function* initializeGameSaga() {
  yield takeLatest([INIT_GAME_START], sagaWorker);
}

export default initializeGameSaga;

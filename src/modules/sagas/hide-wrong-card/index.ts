import {  put, select, takeLatest } from "redux-saga/effects";
import { HIDE_WRONG_PEER_START, hideWrongPeerSuccess } from "../../../modules/actions";
import { ICardImg } from "../../../modules/interfaces/cardImage";
import { getCurrentVisibleCard } from "../../../modules/selectors/current-show-card";
import { getAllCard } from "../../../modules/selectors/get-all-card";

function* sagaWorker(action: any) {
  try {
    const currentVisible: ICardImg[] = yield select(getCurrentVisibleCard);
    const allCard: ICardImg[] = yield select(getAllCard);
    yield currentVisible.map((item) => {
      item['visible'] = false;
      const index: number = allCard.findIndex(elt => elt.id === item.id);
      allCard[index] = item;
      return null;
    });
    yield put(hideWrongPeerSuccess({ currentVisible: [] , newAllCard: allCard }));
  } catch (ex) {
    console.error(ex);
  }
}

function* hideWrongCardSaga() {
  yield takeLatest([HIDE_WRONG_PEER_START], sagaWorker);
}

export default hideWrongCardSaga;

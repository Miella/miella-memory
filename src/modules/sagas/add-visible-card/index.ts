import { takeLatest, select, put } from 'redux-saga/effects';
import { ADD_VISIBLE_CARD_START, AddVisibleCardError, AddVisibleCardSuccess } from '../../actions';
import { getCurrentVisibleCard } from '../../selectors/current-show-card';
import { ICardImg } from '../../../modules/interfaces/cardImage';
import { getAllCard } from '../../../modules/selectors/get-all-card';

function* sagaWorker(action: any) {
  try {
    const { data } = action;
    const currentVisible: ICardImg[] = yield select(getCurrentVisibleCard);
    const allCard: ICardImg[] = yield select(getAllCard);

    // update global list by putting clicked card to visible = true
    yield data['visible'] = true;
    const index: number = yield allCard.findIndex(item => item.id === data.id);
    yield allCard[index] = data;

    // currentVisible is an array of image that awaiting to be compare to another
    // currentVisible should not exceed 2 length
    yield currentVisible.push(data);

    yield put(AddVisibleCardSuccess({ currentVisible , newAllCard: allCard }));
  } catch (ex) {
    console.error(ex);
    yield put(AddVisibleCardError());
  }
}

function* addVisibleCardSaga() {
  yield takeLatest([ADD_VISIBLE_CARD_START], sagaWorker);
}

export default addVisibleCardSaga;

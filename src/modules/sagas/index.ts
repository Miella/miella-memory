import addVisibleCardSaga from './add-visible-card';
import initializeGameSaga from './initialize-game';
import hideWrongCardSaga from './hide-wrong-card';
import validatePeerCardSaga from './validate-peer-card';


export const memorySaga = [
  addVisibleCardSaga(),
  initializeGameSaga(),
  hideWrongCardSaga(),
  validatePeerCardSaga(),
]

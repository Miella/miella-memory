export const INIT_GAME_START = 'memory/INIT_GAME_START';
export const INIT_GAME_SUCCESS = 'memory/INIT_GAME_SUCCESS';
export const INIT_GAME_ERROR = 'memory/INIT_GAME_ERROR';

export const ADD_VISIBLE_CARD_START = 'memory/ADD_VISIBLE_CARD_START';
export const ADD_VISIBLE_CARD_SUCCESS = 'memory/ADD_VISIBLE_CARD_SUCCESS';
export const ADD_VISIBLE_CARD_ERROR = 'memory/ADD_VISIBLE_CARD_ERROR';

export const VALIDATE_PEER_CARD_START = 'memory/VALIDATE_PEER_CARD_START';
export const VALIDATE_PEER_CARD_SUCCESS = 'memory/VALIDATE_PEER_CARD_SUCCESS';
export const VALIDATE_PEER_CARD_ERROR = 'memory/VALIDATE_PEER_CARD_ERROR';

export const HIDE_WRONG_PEER_START = 'memory/HIDE_WRONG_PEER_START';
export const HIDE_WRONG_PEER_SUCCESS = 'memory/HIDE_WRONG_PEER_SUCCESS';
export const HIDE_WRONG_PEER_ERROR = 'memory/HIDE_WRONG_PEER_ERROR';

export const InitGameStart = (data?: any) => ({
  type: INIT_GAME_START, data,
});

export const InitGameSuccess = (data?: any) => ({
  type: INIT_GAME_SUCCESS, data,
});

export const InitGameError = (data?: any) => ({
  type: INIT_GAME_ERROR, data,
});


export const AddVisibleCardStart = (data?: any) => ({
  type: ADD_VISIBLE_CARD_START, data,
});

export const AddVisibleCardSuccess = (data?: any) => ({
  type: ADD_VISIBLE_CARD_SUCCESS, data,
});

export const AddVisibleCardError = (data?: any) => ({
  type: ADD_VISIBLE_CARD_ERROR, data,
});

export const validatePeerCardStart = (data?: any) => ({
  type: VALIDATE_PEER_CARD_START, data,
});

export const validatePeerCardSuccess = (data?: any) => ({
  type: VALIDATE_PEER_CARD_SUCCESS, data,
});

export const validatePeerCardError = (data?: any) => ({
  type: VALIDATE_PEER_CARD_ERROR, data,
});


export const hideWrongPeerStart = (data?: any) => ({
  type: HIDE_WRONG_PEER_START, data,
});

export const hideWrongPeerSuccess = (data?: any) => ({
  type: HIDE_WRONG_PEER_SUCCESS, data,
});

export const hideWrongPeerError = (data?: any) => ({
  type: HIDE_WRONG_PEER_ERROR, data,
});

export interface ICardImg {
  id: string;
  name: string;
  url: string;
  visible: boolean;
}

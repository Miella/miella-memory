import { ICardImg } from '~/modules/interfaces/cardImage';

export default [
      {
        "id":"6026390b-03bd-4d2c-8dec-5db163caed32",
        "name": "image-1",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575497/memory/ryanlerch_boyface8_colour_zmotvi.svg",
        "visible": false,
      },
      {
        "id": "7a9f2031-6af0-4b46-8806-e49090e7c506",
        "name": "image-2",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575497/memory/PeterM_Tree_ekin3n.svg",
        "visible": false,
      },
      {
        "id": "c92eebb9-c6be-4f5e-a833-a31d24189145",
        "name": "image-3",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575495/memory/Gerald_G_Girl_and_Boy_Driving_Car_Cartoon_jz6b8t.svg",
        "visible": false,
      },
      {
        "id": "95b11008-1f73-4ee3-8645-1dec814c0880",
        "name": "image-4",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575495/memory/PeterM_Penguin_with_a_shirt_psasft.svg",
        "visible": false,
      },
      {
        "id": "0dad0f90-3703-402c-b3b2-56cce1c1af78",
        "name": "image-5",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575495/memory/PeterM_Mouse_b5ejh7.svg",
        "visible": false,
      },
      {
        "id": "a060ccd1-cea3-40c9-bac9-fc3914d68fbe",
        "name": "image-6",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575495/memory/BigRedSmile_A_Little_Purple_House_dutan6.svg",
        "visible": false,
      },
      {
        "id": "d5ac9668-e9e9-4578-9975-2de28fd67d39",
        "name": "image-7",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575495/memory/googley-eye-birds_vif6uv.svg",
        "visible": false,
      },
      {
        "id": "dd3a7a1e-85b1-4ceb-a225-807f596cf592",
        "name": "image-8",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575495/memory/PeterM_Flower_jrgkgn.svg",
        "visible": false,
      },
      {
        "id": "a06ce1fd-e7aa-4f15-b154-28c089aa58db",
        "name": "image-1",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575497/memory/ryanlerch_boyface8_colour_zmotvi.svg",
        "visible": false,
      },
      {
        "id": "85bead83-7f0e-44c4-8e6d-86d729dd1e44",
        "name": "image-2",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575497/memory/PeterM_Tree_ekin3n.svg",
        "visible": false,
      },
      {
        "id": "f3e64762-0e70-4f4a-9e95-6ff68f178515",
        "name": "image-3",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575495/memory/Gerald_G_Girl_and_Boy_Driving_Car_Cartoon_jz6b8t.svg",
        "visible": false,
      },
      {
        "id": "654b6d32-ef59-44ae-ac5a-0f1aeb76085c",
        "name": "image-4",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575495/memory/PeterM_Penguin_with_a_shirt_psasft.svg",
        "visible": false,
      },
      {
        "id": "1f9913f3-efd6-47cc-86b5-3c8df14c2f67",
        "name": "image-5",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575495/memory/PeterM_Mouse_b5ejh7.svg",
        "visible": false,
      },
      {
        "id": "00ac98a0-5d1a-443d-9310-cfb94b564622",
        "name": "image-6",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575495/memory/BigRedSmile_A_Little_Purple_House_dutan6.svg",
        "visible": false,
      },
      {
        "id": "da25b5c3-722a-4ce5-9c16-1de0b924950e",
        "name": "image-7",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575495/memory/googley-eye-birds_vif6uv.svg",
        "visible": false,
      },
      {
        "id": "e8425471-e6cc-44a6-91f4-b90603e42a88",
        "name": "image-8",
        "url": "https://res.cloudinary.com/dmje2az94/image/upload/v1648575495/memory/PeterM_Flower_jrgkgn.svg",
        "visible": false,
      }
    ] as ICardImg[];

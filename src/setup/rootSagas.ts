import { all } from 'redux-saga/effects';
import { memorySaga } from '../modules/sagas';

function* rootSaga () {
  yield all([
    ...memorySaga,
  ]);
}

export default rootSaga;

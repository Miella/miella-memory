import { combineReducers } from 'redux';
import memoryReducer from '../modules/reducer';

const reducers = {
  app: memoryReducer,
};

const appReducer = combineReducers(reducers);

export type IApplicationState = ReturnType<typeof appReducer>;

export default appReducer;

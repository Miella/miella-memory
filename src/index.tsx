import React from 'react';
import ReactDOM from 'react-dom';
import Memory from './components';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './setup/reducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { Provider } from 'react-redux';
import rootSaga from './setup/rootSagas';

const sagaMiddleware = createSagaMiddleware();

const devTools =
  process.env.NODE_ENV === "production"
    ? applyMiddleware(sagaMiddleware)
    : composeWithDevTools(applyMiddleware(sagaMiddleware));

const store = createStore(
  rootReducer,
  devTools
);

sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <Provider store={store}>
    <Memory />
  </ Provider>
  ,
  document.getElementById('root')
);
